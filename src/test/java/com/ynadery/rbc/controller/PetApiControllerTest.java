package com.ynadery.rbc.controller;

import com.ynadery.rbc.domain.PetItem;
import com.ynadery.rbc.dto.PetItemDto;
import com.ynadery.rbc.service.PetItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * @author Yashar Hn.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PetApiControllerTest {

    @MockBean
    private PetItemService petItemService;

    @Autowired
    private PetApiController controller;

    @Test
    public void testFindById() throws Exception {
        given(this.petItemService.findPetItem(10l)).willReturn(new PetItem(10l, "Pet 10"));
        PetItemDto petItemDto = controller.findById(10l);
        assertNotNull(petItemDto);
        assertEquals(petItemDto.getName(), "Pet 10");
    }
}