package com.ynadery.rbc;

import com.ynadery.rbc.controller.PetApiController;
import com.ynadery.rbc.domain.PetItem;
import com.ynadery.rbc.dto.PetItemDto;
import com.ynadery.rbc.exception.NotFoundException;
import com.ynadery.rbc.service.PetItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PetStoreApplicationTests {

	@Test
	public void contextLoads() {
	}

}
