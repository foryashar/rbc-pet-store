package com.ynadery.rbc.controller;

import com.ynadery.rbc.domain.PetItem;
import com.ynadery.rbc.dto.PetItemDto;
import com.ynadery.rbc.exception.NotFoundException;
import com.ynadery.rbc.service.PetItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Yashar Hn.
 */
@RestController
@Controller
@RequestMapping("/pet")
public class PetApiController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PetItemService petItemService;


    @RequestMapping(value="/{petId}",method = RequestMethod.GET)
    public PetItemDto findById(@PathVariable Long petId) throws NotFoundException, IllegalArgumentException {
        PetItem petItem = petItemService.findPetItem(petId);
        if(petItem==null){
            throw new NotFoundException("Pet item is not found:" + petId);
        }
        return convertToDto(petItem);
    }

    @RequestMapping(method = RequestMethod.POST)
    public PetItemDto addNewPet(@RequestBody PetItemDto petItemDto) throws IllegalArgumentException {
        PetItem petItem = convertToEntity(petItemDto);
        petItem = petItemService.createPetItem(petItem);
        if(petItem==null){
            throw new IllegalArgumentException("Pet item was not created");
        }
        return convertToDto(petItem);
    }

    @RequestMapping(value="/{petId}",method = RequestMethod.DELETE)
    public boolean deleteById(@PathVariable Long petId) throws NotFoundException{
        boolean result = petItemService.deletePetItem(petId);
        if(!result){
            throw new NotFoundException("Can not delete Pet Item with id:" + petId);
        }
        return true;
    }

    @RequestMapping(value="/findAll",method = RequestMethod.GET)
    public Page<PetItemDto> findAll(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageNum,
            @RequestParam(value = "size", required = false, defaultValue = "20") Integer pageSize
    ){
        Page<PetItem> petItemPage = petItemService.findAllPetItems(pageNum, pageSize);

        Page<PetItemDto> petItemDtoPage = new PageImpl<>(convertToDtoList(petItemPage.getContent()),
                new PageRequest(pageNum, pageSize), petItemPage.getTotalElements());
        return petItemDtoPage;
    }


    private PetItemDto convertToDto(PetItem pet) {
        PetItemDto petItemDto = null;
        if(pet!=null){
            petItemDto = modelMapper.map(pet, PetItemDto.class);
        }
        return petItemDto;
    }

    private List<PetItemDto> convertToDtoList(List<PetItem> petItems) {
        List<PetItemDto> petItemDtoList = new ArrayList<>();
        if(petItems!=null){
            for(PetItem petItem: petItems){
                PetItemDto petItemDto = convertToDto(petItem);
                if(petItemDto!=null){
                    petItemDtoList.add(petItemDto);
                }
            }
        }
        return petItemDtoList;
    }

    private PetItem convertToEntity(PetItemDto petDto) throws ParseException {
        PetItem petItem = null;
        if(petDto!=null){
            petItem = modelMapper.map(petDto, PetItem.class);
        }
        return petItem;
    }

    @ExceptionHandler({NotFoundException.class})
    void handleNotFoundException(HttpServletResponse response) throws IOException {
        //response.setStatus(HttpStatus.NOT_FOUND.value());
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({IllegalArgumentException.class})
    void handleIllegalArgumentException(HttpServletResponse response) throws IOException {
        //response.setStatus(HttpStatus.NOT_FOUND.value());
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}