package com.ynadery.rbc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Yashar Hn.
 */
@Controller
public class PetPageController {

    @GetMapping("/item/{petItemId}")
    String showItem(
            @PathVariable("petItemId") Long petItemId,
            Model model) {

        model.addAttribute("petItemId", petItemId);
        return "item";
    }
}
