package com.ynadery.rbc.service;

import com.ynadery.rbc.domain.PetItem;
import org.springframework.data.domain.Page;

/**
 * @author Yashar Hn.
 */
public interface PetItemService {

    PetItem createPetItem(PetItem petItem);

    PetItem updatePetItem(PetItem newPetItem);

    PetItem findPetItem(Long id) throws IllegalArgumentException ;

    boolean deletePetItem(Long id);

    Page<PetItem> findAllPetItems(int page, int size);
}
