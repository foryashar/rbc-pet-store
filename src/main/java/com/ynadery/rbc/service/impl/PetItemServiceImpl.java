package com.ynadery.rbc.service.impl;

import com.ynadery.rbc.dao.CategoryRepository;
import com.ynadery.rbc.dao.PetItemRepository;
import com.ynadery.rbc.dao.TagRepository;
import com.ynadery.rbc.domain.Category;
import com.ynadery.rbc.domain.PetItem;
import com.ynadery.rbc.domain.Tag;
import com.ynadery.rbc.service.PetItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Yashar Hn.
 */
@Service
public class PetItemServiceImpl implements PetItemService {

    @Autowired
    private PetItemRepository petItemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TagRepository tagRepository;

    public PetItem createPetItem(PetItem newPetItem){
        PetItem petItem = null;
        if(newPetItem!=null){
            petItem = new PetItem(null, newPetItem.getName());
            mergeAndSave(petItem, newPetItem);
        }
        return petItem;
    }

    public PetItem updatePetItem(PetItem newPetItem){
        PetItem petItem = null;
        if(newPetItem!=null && newPetItem.getId()!=null){
            petItem = petItemRepository.getOne(newPetItem.getId());
            mergeAndSave(petItem, newPetItem);
        }
        return petItem;
    }

    public PetItem findPetItem(Long id) throws IllegalArgumentException {
        PetItem petItem = null;
        if(id!=null){
            petItem = petItemRepository.findOne(id);
        } else {
            throw new IllegalArgumentException("PetItem id is null");
        }
        return petItem;
    }

    public boolean deletePetItem(Long id){
        PetItem petItem = findPetItem(id);
        if(petItem!=null){
            petItemRepository.delete(petItem);
            return true;
        }
        return false;
    }

    public Page<PetItem> findAllPetItems(int page, int size){
        return petItemRepository.findAll(new PageRequest(page, size));
    }

    private void mergeAndSave(PetItem petItem, PetItem newPetItem){
        if(petItem!=null && newPetItem!=null){
            if(newPetItem.getCategory()!=null){
                Category category = null;
                if(newPetItem.getCategory().getId()!=null){
                    category = categoryRepository.findOne(newPetItem.getCategory().getId());
                }
                if(category==null && StringUtils.isNotEmpty(newPetItem.getCategory().getName())){
                    category = categoryRepository.findByName(newPetItem.getCategory().getName());
                }
                if(category==null){
                    category = newPetItem.getCategory();
                    category.setId(null);
                    categoryRepository.save(category);
                }
                petItem.setCategory(category);
            }
            if(!CollectionUtils.isEmpty(newPetItem.getTags())){
                for(Tag tag : newPetItem.getTags()){
                    if(tag!=null){
                        Tag newTag = null;
                        if(tag.getId()!=null){
                            newTag = tagRepository.findOne(tag.getId());
                        }
                        if(newTag==null && StringUtils.isNotEmpty(tag.getName())){
                            newTag = tagRepository.findByName(tag.getName());
                        }
                        if(newTag==null){
                            newTag = tag;
                            newTag.setId(null);
                            tagRepository.save(newTag);
                        }
                        petItem.addTag(newTag);
                    }
                }
            }
            if(!CollectionUtils.isEmpty(newPetItem.getPhotoUrls())){
                for(String photoUrls : newPetItem.getPhotoUrls()){
                    petItem.addPhotoUrl(photoUrls);
                }
            }
            petItem.setStatus(newPetItem.getStatus());

            petItemRepository.saveAndFlush(petItem);
        }
    }
}
