package com.ynadery.rbc.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Yashar Hn.
 */
public enum ItemStatus {
    AVAILABLE,
    PENDING,
    SOLD;

    @JsonCreator
    public static ItemStatus fromString(String key) {
        return key == null
                ? null
                : ItemStatus.valueOf(key.toUpperCase());
    }

    @JsonValue
    public String getKey() {
        return this.name().toLowerCase();
    }
}
