package com.ynadery.rbc.dao;

import com.ynadery.rbc.domain.PetItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Yashar Hn.
 */
public interface PetItemRepository extends JpaRepository<PetItem, Long> {

    Page<PetItem> findAll(Pageable pageable);


}
