package com.ynadery.rbc.exception;

/**
 * @author Yashar Hn.
 */
public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
