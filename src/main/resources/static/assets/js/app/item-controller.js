storeApp.controller('productCtrl', function($scope, $http) {
    $scope.item = {};
    $scope.petItemId = $('#petItemId').val();

    $http.get("/pet/" + $scope.petItemId)
        .then(function(response) {
            if(response.data){
                $scope.item = response.data;
            }
        });

    $scope.deleteItem = function(){
        if (confirm("Are you sure?")) {
            $http.delete("/pet/" + $scope.petItemId)
                .then(function(response) {
                    if(response.data){
                        $.notify("Item has been deleted successfully!", "success");
                        window.setTimeout( function(){
                                         window.location = "/all-items.html";
                                     }, 5000 );
                    } else {
                        $.notify("Failed!");
                    }
                });
        }
    }
});