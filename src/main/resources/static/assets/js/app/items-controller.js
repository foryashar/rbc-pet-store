storeApp.controller('productsCtrl', function($scope, $http) {
    $scope.items = [];

    $http.get("/pet/findAll")
        .then(function(response) {
            if(response.data && response.data.content){
                $scope.items = response.data.content;
            }
        });
});